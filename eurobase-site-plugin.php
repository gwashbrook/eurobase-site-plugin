<?php
/*
Plugin Name: Eurobase Site Plugin
Description: Contains theme independent functionality for Eurobase Mobile Homes
Plugin URI: http://my.powerhut.net/
Author: Graham Washbrook
Author URI: http://powerhut.tel
Text Domain: plugin-text-domain
Domain Path: /languages
Version: 0.0.2
*/

//* Search and Replace Strings
// namespace = ns_
// defined namespace path = NS_PATH ~ e.g. EBMHSP_PATH

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//* Define plugin path
define( 'EBMHSP_PATH', plugin_dir_path( __FILE__ ) );


add_action ('init','ebmhsp_init');

function ebmhsp_init() {

	// Display the addons even when the purchase visibility setting is enabled
	add_action( 'catalog_visibility_before_alternate_add_to_cart_button', array( $GLOBALS['Product_Addon_Display'], 'display' ), 10 );

}